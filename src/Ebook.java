import java.util.HashMap;


public class Ebook {
	private String name;
	//change this to 4 later
	private HashMap<Integer, Page> pagesHash = new HashMap<Integer, Page>();
	//private Page[] pages = new Page[4];
	public Ebook(String title, Page pageContent, Integer pageNum) {
		this.name = title;
		this.pagesHash.put(pageNum, pageContent);
	}

public String getTitle() {
	return this.name;
}

public Page getPage(int num) {
	Page returnPage = null;
	returnPage = this.pagesHash.get(num);
	return returnPage;
}

public void addPage(int pageNum, Page pageContent) {
	this.pagesHash.put(pageNum, pageContent);
}
}
