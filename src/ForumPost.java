
public class ForumPost {
	private String comment;
	private Integer serialID;
	private String user;
	private String hashID;
	public ForumPost(String comment, Integer ID, String name, String hashID) {
		this.comment = comment;
		this.serialID = ID;
		this.user = name;
		this.hashID = hashID;
	}
	
	public String getHashID () {
		return this.hashID;
	}
	
	public String getComment () {
		return this.comment;
	}
	
	public Integer getID () {
		return this.serialID;
	}
	
	public String getUser () {
		return this.user;
	}
}
