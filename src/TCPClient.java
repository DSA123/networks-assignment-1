/*
 *
 *  client for TCPClient from Kurose and Ross
 *
 *  * Usage: java TCPClient [server addr] [server port]
 */
import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

public class TCPClient {

	static Timer timer;
	static HashMap<String, ArrayList<ForumPost>> readerDB = new HashMap<String, ArrayList<ForumPost>>();
	static Socket pushSocket;
	public static void main(String[] args) throws Exception {

		// get server address
		String serverName = "localhost";
		String userName = "USERNAME";
		Integer pullTime = 20;
		String mode = "";
		//Timer timer = new Timer();
		if (args.length >= 2) {
			userName = args[0];
			mode = args[1];
		}
		
		//if (args.length >= 1)
		//    serverName = args[0];
		InetAddress serverIPAddress = InetAddress.getByName(serverName);

		// get server port
		int serverPort = 6789; 
		//change above port number if required
		//if (args.length >= 2)
		  //  serverPort = Integer.parseInt(args[1]);
		
		//HashMap<String, ArrayList<ForumPost>> readerDB = new HashMap<String, ArrayList<ForumPost>>();
		
		
		String currentBook = "None";
		Integer currentPage = 0;
		Integer lineNumber = 0;
		if (mode.equals("pull")) {
			
			while (true) {
				
				// get input from keyboard
				
				String sentence;
				BufferedReader inFromUser = new BufferedReader(new InputStreamReader(System.in));
				sentence = inFromUser.readLine();
				
				String[] words = sentence.split("\\s+");
				if (words.length > 0 && words[0] != null && words[0].equals("display")) {
					if (words.length == 3 && words[1] != null && words[2] != null) {
						currentBook = words[1];
						currentPage = Integer.parseInt(words[2]);
						
						setTimer(pullTime, currentBook, currentPage, userName, serverIPAddress, serverPort);
						sentence = sentence + " " + userName;
					}
				} else if (words.length >= 3 && words[0] != null && words[0].equals("post_to_forum")) {
					sentence = sentence + " " + currentBook + " " + currentPage + " " + userName;
				} else if (words.length == 2 && words[0] != null && words[0].equals("read_post")) {
					sentence = sentence + " " + currentBook + " " + currentPage;
					if (words[1] != null) lineNumber = Integer.parseInt(words[1]);
				}
				// create socket which connects to server
				Socket clientSocket = new Socket(serverIPAddress, serverPort);
	
	
				// write to server
				DataOutputStream outToServer = new DataOutputStream(clientSocket.getOutputStream());
				outToServer.writeBytes(sentence + '\n');
	
				// create read stream and receive from server
				BufferedReader inFromServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
				String sentenceFromServer;
				sentenceFromServer = inFromServer.readLine();
				
				String copySentence = sentenceFromServer;
				String[] wordsFromServer = copySentence.split("/n");
				
				
				if (sentenceFromServer.equals("None")) {
					currentBook = sentenceFromServer;
					currentPage = 0;
					System.out.println("From Server: " + sentenceFromServer);
				} else if (wordsFromServer[0] != null && wordsFromServer[0].equals("post_summary")) {
					String hashID = currentBook + "-" + currentPage + "-" + lineNumber;
					String outBook = currentBook.substring(0, 1).toUpperCase() + currentBook.substring(1);
					
					String outputSentence = "Book by " + outBook + ", Page " + currentPage + ", Line number " + lineNumber + ":";
					System.out.println(outputSentence);
					
					if (readerDB.containsKey(hashID) == false) {
						ArrayList<ForumPost> newList = new ArrayList<ForumPost>();
						readerDB.put(hashID, newList);
					}
					for (String s: wordsFromServer) {
						if (s.equals("post_summary")) {
							continue;
						} else {
							String firstPart = s.replaceFirst("-.*", "");
							String postersName = s.replaceFirst("[^-]*-", "");
							postersName = postersName.replaceFirst("-.*", "");
							String secondPart = s.replaceFirst("[^-]*-[^-]*-", "");
							Boolean seen = false;
							for (ForumPost f: readerDB.get(hashID)) {
								if (f.getID() == Integer.parseInt(firstPart)) {
									seen = true;
									break;
								}
							}
							if (seen == false) {
								ForumPost newPost = new ForumPost(secondPart, Integer.parseInt(firstPart), userName, hashID);
								readerDB.get(hashID).add(newPost);
								System.out.println(newPost.getID() + " " + postersName + ": " + newPost.getComment());
							}
							//System.out.println(firstPart + " " + secondPart);
						}
					}
					
				} else {
				
				String processSentence;
				processSentence = sentenceFromServer.replaceAll("/n", "\n");
				String[] lines = processSentence.split("\n");
				//String[] bookLines = lines;
				ArrayList<String> readerPosts = new ArrayList<String>();
				ArrayList<String> serverPosts = new ArrayList<String>();
				
				for (Integer i = 1; i <= 9; i++) {
					String hashID = currentBook + "-" + currentPage + "-" + i;
					if (readerDB.containsKey(hashID)) {
						for (ForumPost f: readerDB.get(hashID)) {
							String newString = f.getID() + "-" + i;
							readerPosts.add(newString);
						}
					}
				}
				
				if (words.length > 0 && words[0] != null && words[0].equals("display")) {
					
					int count = 0;
					for (String s: lines) {
						if (count < 9) {
							String hashID = currentBook + "-" + currentPage + "-" + (count+1);
							 
							
							// if readerDB contains hashID
							// get the array list of forum posts for each hashID and add them all to readerPosts
							if (readerDB.containsKey(hashID)) {
								if (lines.length >= count && lines[count] != null) {
									lines[count] = lines[count].replaceFirst(".", "m");
								}
							}
						} else {
							// make an array list of all the forum post ID's and their page number (format: ID-pageNum) (from the reader DB).
							// create a second array list containing all the forum post ID's and page numbers for 
							// each string in lines that is a forum post ID and page number (lines[9-infinity]) (from the server DB).
							// then remove all elements from serverDB list contained in readerDB list.
							// we can then iterate through the reader list and replaceFirst("." "m") for each line number found.
							// we can then iterate through the server list and replaceFirst(".", "n") for each line number found.
							// This way we have all the read line numbers starting with "m", and for any line that contains an unread post, the "m" will instead be "n".
							// If the reader list does not contain the post, then the blank " " at the start of the line will be replaced with "n" as intended anyway.
							serverPosts.add(s);
							//System.out.println(s);
							
						}
						count++;
					}
					/*
					System.out.println("Server posts are:");
					for (String print: serverPosts) {
						System.out.println(print);
					}
					System.out.println("Reader posts are:");
					for (String printTwo: readerPosts) {
						System.out.println(printTwo);
					}
					*/
					serverPosts.removeAll(readerPosts);
					
					if (serverPosts.isEmpty() == false) {
						for (String post: serverPosts) {
							String[] IdAndNum = post.split("-");
							Integer number = Integer.parseInt(IdAndNum[1]);
							lines[number-1] = lines[number-1].replaceFirst(".", "n");
						}
					}
					
				}
				processSentence = "";
				int countTwo = 0;
				for (String s: lines) {
					if (countTwo < 9) {
						processSentence += s + '\n';
					} else {
						break;
					}
					countTwo++;
				}
				// print output
				System.out.println("From Server: " + "\n" + processSentence);
				}
				
				if (words.length > 0 && words[0] != null && words[0].equals("display")) {
					if (words.length == 3 && words[1] != null && words[2] != null) {
						System.out.println("Currently viewing " + currentBook + " on page " + currentPage);
					}
				}
				// close client socket
				clientSocket.close();
			}
		} else if (mode.equals("push")) {
			
			HashMap<String, ArrayList<ForumPost>> unreadDB = new HashMap<String, ArrayList<ForumPost>>();
			String sentence = "push " + userName;
			Socket initSocket = new Socket(serverIPAddress, serverPort);
			
			// write to server
			DataOutputStream outToServer = new DataOutputStream(initSocket.getOutputStream());
			outToServer.writeBytes(sentence + '\n');

			// create read stream and receive from server
			BufferedReader inFromServer = new BufferedReader(new InputStreamReader(initSocket.getInputStream()));
			String sentenceFromServer;
			sentenceFromServer = inFromServer.readLine();
			
			if (sentenceFromServer.equals("No posts exist")) {
				System.out.println(sentenceFromServer);
			} else {
				//sentenceFromServer.replaceAll("/n", "\n");
				String[] posts = sentenceFromServer.split("/n");
				for (String p: posts) {
					
					
					String hashID = p.replaceFirst("-[0-9]+-[A-Z]{1}.*", "");
					String postID = p.replaceFirst("[^-]*-[^-]*-[^-]*-", "");
					postID = postID.replaceFirst("-.*", "");
					String posterName = p.replaceFirst("[^-]*-[^-]*-[^-]*-[^-]*-", "");
					posterName = posterName.replaceFirst("-.*", "");
					String postContent = p.replaceFirst("[^-]*-[^-]*-[^-]*-[^-]*-[^-]*-", "");
					
					
					ForumPost newPost = new ForumPost(postContent, Integer.parseInt(postID), posterName, hashID);
					if (unreadDB.containsKey(hashID)) {
						unreadDB.get(hashID).add(newPost);
					} else {
						ArrayList<ForumPost> newList = new ArrayList<ForumPost>();
						newList.add(newPost);
						unreadDB.put(hashID, newList);
					}
				}
				// do something
			}
			initSocket.close();
			
			// WE HAVE NOW SUCCESSFULLY ADDED ALL POSTS TO THE DATABASE
			
			System.out.println("Successfully initialised local post database with the following posts:");
			for (ArrayList<ForumPost> a: unreadDB.values()) {
				for (ForumPost f: a) {
					System.out.println(f.getHashID());
					System.out.println(f.getID() + " " + f.getUser() + ": " + f.getComment());
				}
			}
			
			pushSocket = new Socket(serverIPAddress, serverPort);
			BufferedReader readerInput = new BufferedReader(new InputStreamReader(pushSocket.getInputStream()));
			while (true) {
				
				
				if (readerInput.ready()) {
					
					System.out.println("New post added to database");
					String pushedPost = readerInput.readLine();
					String[] postWords = pushedPost.split("-");
					String postHash = postWords[0] + "-" + postWords[1] + "-" + postWords[2];
					System.out.println(postHash);
					Integer postID = Integer.parseInt(postWords[3]);
					String postUser = postWords[4];
					String postContent = "";
					Integer wordCount = 0;
					for (String s: postWords) {
						if (wordCount <= 4) {
							wordCount++;
							continue;
						}
						postContent += s;
					}
					/*
					System.out.println("Post hash is " + postHash);
					System.out.println("PostID is " + postID);
					System.out.println("Post User is " + postUser);
					System.out.println("Post content is: " + postContent);
					*/
					ForumPost addedPost = new ForumPost(postContent, postID, postUser, postHash);
					//System.out.println(postHash);
					if (!unreadDB.containsKey(postHash)) {
						ArrayList<ForumPost> newPosts = new ArrayList<ForumPost>();
						unreadDB.put(postHash, newPosts);
					}
						unreadDB.get(postHash).add(addedPost);
						
						String postBookTitle = postWords[0];
						Integer postBookPage = Integer.parseInt(postWords[1]);
						if (postBookTitle.equals(currentBook) && postBookPage.equals(currentPage)) {
							// Check if new post is on current book and page (we don't care about line no.)
							System.out.println("There are new posts");
						}
					/*
					for (ArrayList<ForumPost> postList: unreadDB.values()) {
						System.out.println("Printing all unread posts");
						for (ForumPost post: postList) {
							System.out.println("Post hash is " + post.getHashID());
							System.out.println("PostID is " + post.getID());
							System.out.println("Post User is " + post.getUser());
							System.out.println("Post content is: " + post.getComment());
						}
					}
					*/
					// process forum post
				}
				
				BufferedReader inFromUser = new BufferedReader(new InputStreamReader(System.in));
				
				
				if (!inFromUser.ready()) {
					continue;
				}
				
				sentence = inFromUser.readLine();
				String words[] = sentence.split("\\s+");
				if (words.length > 0 && words[0] != null && words[0].equals("display")) {
					if (words.length == 3 && words[1] != null && words[2] != null) {
						currentBook = words[1];
						currentPage = Integer.parseInt(words[2]);
						
						sentence = sentence + " " + userName + " " + "push";
					}
				} else if (words.length >= 3 && words[0] != null && words[0].equals("post_to_forum")) {
					sentence = sentence + " " + currentBook + " " + currentPage + " " + userName;
				} else if (words.length == 2 && words[0] != null && words[0].equals("read_post")) {
					sentence = sentence + " " + currentBook + " " + currentPage;
					if (words[1] != null) lineNumber = Integer.parseInt(words[1]);
					String readHash = currentBook + "-" + currentPage + "-" + lineNumber;
					
					String outBook = currentBook.substring(0, 1).toUpperCase() + currentBook.substring(1);
					
					System.out.println("Book by " + outBook + ", Page " + currentPage + ", Line number " + lineNumber + ":");
					//System.out.println(readHash);
					if (unreadDB.containsKey(readHash)) {
						for (ForumPost f: unreadDB.get(readHash)) {
							System.out.println(f.getID() + " " + f.getUser() + ": " + f.getComment());
						}
						ArrayList<ForumPost> unreadPosts = unreadDB.get(readHash);
						
						if (readerDB.containsKey(readHash)) {
							readerDB.get(readHash).addAll(unreadPosts);
						} else {
							readerDB.put(readHash, unreadPosts);
						}
						unreadDB.remove(readHash);
					} else {
						System.out.println("No unread posts found");
					}
					
					continue;
				}
				
				Socket clientSocket = new Socket(serverIPAddress, serverPort);
				
				// write to server
				outToServer = new DataOutputStream(clientSocket.getOutputStream());
				outToServer.writeBytes(sentence + '\n');

				// create read stream and receive from server
				inFromServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
				sentenceFromServer = inFromServer.readLine();
				String[] wordsFromServer = sentenceFromServer.split("/n");
				if (words.length > 0 && words[0] != null && words[0].equals("display")) {
					for (Integer lineCount = 1; lineCount <= 9; lineCount++) {
						String currentHashID = currentBook + "-" + currentPage + "-" + lineCount.toString();
						
						if (readerDB.containsKey(currentHashID)) {
							wordsFromServer[lineCount-1] = wordsFromServer[lineCount-1].replaceFirst(".", "m");
						}
						if (unreadDB.containsKey(currentHashID)) {
							wordsFromServer[lineCount-1] = wordsFromServer[lineCount-1].replaceFirst(".", "n");
						}
					}
				}
				
				for (String l: wordsFromServer) {
					System.out.println(l);
				}
				
				if (words.length > 0 && words[0] != null && words[0].equals("display")) {
					if (words.length == 3 && words[1] != null && words[2] != null) {
						System.out.println("Currently viewing " + currentBook + " on page " + currentPage);
					}
				}
				clientSocket.close();
			}
		}

	} // end of main
	
	
	private static void setTimer(Integer time, final String currentBook, final Integer pageNum, final String username, final InetAddress serverIPAddress, final Integer serverPort) {
		// set timer here
		if (timer != null) {
			timer.cancel();
			timer.purge();
			System.out.println("Timer reset");
		}
		timer = new Timer();
		timer.scheduleAtFixedRate(new TimerTask() {
			public void run() {
				try {
					System.out.println("Checking for new posts");
					String sentence = "display " + currentBook + " " + pageNum + " " + username;
					
					Socket clientSocket;
					clientSocket = new Socket(serverIPAddress, serverPort);
					
					// write to server
					DataOutputStream outToServer = new DataOutputStream(clientSocket.getOutputStream());
					outToServer.writeBytes(sentence + '\n');

					// create read stream and receive from server
					BufferedReader inFromServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
					String sentenceFromServer;
					sentenceFromServer = inFromServer.readLine();
					
					String copySentence = sentenceFromServer;
					String[] wordsFromServer = copySentence.split("/n");
					
					if (wordsFromServer.length < 10) {
						System.out.println("No posts found");
					} else {
						
						ArrayList<String> readerPosts = new ArrayList<String>();
						ArrayList<String> serverPosts = new ArrayList<String>();
						
						for (Integer i = 1; i <= 9; i++) {
							String hashID = currentBook + "-" + pageNum + "-" + i;
							if (readerDB.containsKey(hashID)) {
								for (ForumPost f: readerDB.get(hashID)) {
									String newString = f.getID() + "-" + i;
									readerPosts.add(newString);
								}
							}
						}
						Integer count = 0;
						for (String s: wordsFromServer) {
							if (count >= 9) {
								serverPosts.add(s);
							}
							
							count++;
						}
						
						serverPosts.removeAll(readerPosts);
		
						if (serverPosts.isEmpty() == true) {
							System.out.println("No posts found");
						} else {
							System.out.println("There are new posts");
						}
					}
					clientSocket.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} finally {
					// done
				}
			}
		}, 1000 * time, 1000 * time);
	}

} // end of class TCPClient