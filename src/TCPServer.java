/*
 *
 * tcpServer from Kurose and Ross
 *
 * Usage: java TCPServer [server port]
 */

import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class TCPServer {

	public static void main(String[] args)throws Exception {

		//initialise Ebook database from files
		Integer postCount = 0;
		HashMap<String, ArrayList<ForumPost>> serverDB = new HashMap<String, ArrayList<ForumPost>>();
		HashMap<String, Ebook> ebooksHash = new HashMap<String, Ebook>();
		ArrayList<Socket> pushUsers = new ArrayList<Socket>();
		ForumPost recentPost = null;
		String title = "";
		File pagePath = new File("src/eBook-pages");
		File[] pageFiles = pagePath.listFiles();
		for (File f: pageFiles) {
			if (f.isFile()) {
				if (f.getName().equals(".DS_Store")) {
					continue;
				} else {
					
					String fileInfo[] = f.getName().split("_page");
					String currBook = fileInfo[0];
					int currPage = Integer.parseInt(fileInfo[1]);
					
					Scanner bookReader = new Scanner(new File(f.getAbsolutePath()));
					StringBuilder pageBuilder = new StringBuilder();
					while (bookReader.hasNext()) {
						pageBuilder.append(bookReader.nextLine());
						pageBuilder.append("\n");
					}
					bookReader.close();
					String[] pageLines = pageBuilder.toString().split("\\n");
					
					Page newPage = new Page(currPage, pageLines);
					//Page [] newPages = new Page[4];
					//newPages[0] = newPage;
					
					if (title.equals(currBook)) {
						ebooksHash.get(currBook).addPage(currPage, newPage);
						// create and add a new page
					} else {
						Ebook newBook = new Ebook(currBook, newPage, currPage);
						ebooksHash.put(currBook, newBook);
						// create a new book
						// add a new page
					}
					title = currBook;
				}
			}
		}
		// NOW WE HAVE LOOP OF FILE NAMES
		// loop through files of pages
		// check if title matches current title
		// if not, create a new book with that name
		// if so, create a new page and add that to the book
		
		// see if we do not use default server port
		int serverPort = 6789; 
		/* change above port number this if required */
		
		if (args.length >= 1)
		    serverPort = Integer.parseInt(args[0]);
	    
		// create server socket
		ServerSocket welcomeSocket = new ServerSocket(serverPort);

		while (true) {

		    // accept connection from connection queue
		    Socket connectionSocket = welcomeSocket.accept();
		    System.out.println("connection from " + connectionSocket);

		    // create read stream to get input
		    BufferedReader inFromClient = new BufferedReader(new InputStreamReader(connectionSocket.getInputStream()));
		    String clientSentence;
		    clientSentence = inFromClient.readLine();
		    
		    System.out.println(clientSentence);
		    String[] words = clientSentence.split("\\s+"); 
		    
		    String outputSentence = "";
		    
		    
		    if (words.length > 0 && words[0] != null & words[0].equals("display")) {
		    	//display command
		    	if (words.length >= 4 && words[1] != null && words[2] != null && words[3] != null) {
		    		String bookTitle = words[1];
		    		int pageNumber = Integer.parseInt(words[2]);
		    		String readerUsername = words[3];	
		    	
		    		if (ebooksHash.containsKey(bookTitle)) {
			    		Page currentPage = ebooksHash.get(bookTitle).getPage(pageNumber);
			    		if (currentPage != null) {
			    			StringBuilder concatPage = new StringBuilder();
			    			for(String line: currentPage.getLines()) {
			    				concatPage.append(line);
			    				concatPage.append("\n");
			    			}
			    			if (words.length == 4) {
			    				System.out.println("Query from: " + readerUsername + " for posts in: " + bookTitle + " " + pageNumber);
				    			for (Integer i = 1; i < 10; i++) {
									String hashID = bookTitle + "-" + pageNumber + "-" + i;
									if (serverDB.containsKey(hashID)) {
										for (ForumPost f: serverDB.get(hashID)) {
											concatPage.append(f.getID());
											concatPage.append("-");
											concatPage.append(i);
											concatPage.append("\n");
										}
											
									}
								}
			    			}
			    			outputSentence = concatPage.toString().replaceAll("\n", "/n")+'\n';
			    		} else {
			    			outputSentence = "None" +'\n';
			    		}
			    	} else {
			    		outputSentence = "None" +'\n';
			    	}
		    	} else {
		    		outputSentence = clientSentence + '\n';
		    	}
		    } else if (words.length >= 3 && words[0] != null && words[0].equals("post_to_forum")) {
					if (words[1] != null && words[2] != null && Integer.parseInt(words[1]) <= 9 && Integer.parseInt(words[1]) >= 0) {
						Integer lineNum = Integer.parseInt(words[1]);
						
						String contentOfPost = clientSentence.replaceFirst("^post_to_forum [0-9]{1} ","");
						String[] contentList = contentOfPost.split(" ");
						contentOfPost = contentOfPost.replaceAll("\\s[^0-9\\s]+\\s[0-9] [a-zA-Z]+$", "");
						
						if (contentList.length >= 2) {
							String readerUsername = contentList[contentList.length-1];
							Integer userPageNum = Integer.parseInt(contentList[contentList.length-2]);
							String userBook = contentList[contentList.length-3];
							String hashID = userBook + "-" + userPageNum + "-" + lineNum;
							System.out.println("New post received from " + readerUsername + ".");
							
							ForumPost newPost = new ForumPost(contentOfPost, postCount++, readerUsername, hashID);
							System.out.println("Post added to the database and given serial number(" + userBook + ", " + userPageNum + ", " + lineNum + ", " + newPost.getID() + ").");
							// check if ArrayList exists for hashID, if not create array list then add new post
							// else just add new post
							
							if (serverDB.containsKey(hashID) == false) {
								ArrayList<ForumPost> pageCommentList = new ArrayList<ForumPost>();
								serverDB.put(hashID, pageCommentList);
							}
							
							serverDB.get(hashID).add(newPost);
							
							recentPost = newPost;
							//System.out.println(userBook + " " + userPageNum);
						}
						
						
						//add content of post to data structure of ForumPosts for the book, page and line number.
						outputSentence = contentOfPost + '\n';
					} else {
						outputSentence = "Could not post to server" + '\n';
					}
		    } else if (words.length == 4 && words[0] != null && words[1] != null && words[2] != null && words[3] != null && words[0].equals("read_post")) { 
		    	
		    	Integer lineNum = Integer.parseInt(words[1]);
		    	
				String[] readList = clientSentence.split(" ");
			
				Integer userPageNum = Integer.parseInt(readList[readList.length-1]);
				String userBook = readList[readList.length-2];
				String hashID = userBook + "-" + userPageNum + "-" + lineNum;
				
				if (serverDB.containsKey(hashID)) {
			    	outputSentence = "post_summary\n";
					for (ForumPost f: serverDB.get(hashID)) {
			    		String outComment = f.getID().toString() + "-" + f.getUser() + "-" + f.getComment();
						outputSentence += outComment + '\n';
			    		//System.out.println(outComment);
					}
					outputSentence = outputSentence.replaceAll("\n", "/n") + '\n';
				} else {
					outputSentence = "No posts found" + '\n';
				}
		    } else if (words.length == 2 && words[0] != null && words[1] != null && words[0].equals("push")) {
		    		System.out.println("Added " + words[1] + " to push list");
		    		ArrayList<ForumPost> allPosts = new ArrayList<ForumPost>();
		    		for (Ebook e: ebooksHash.values()) {
		    			for (Integer i = 1; i <= 4; i++) {
		    				for (Integer j = 1; j <= 9; j++) {
		    					String hashID = e.getTitle() + "-" + i + "-" + j;
		    					if (serverDB.containsKey(hashID)) {
		    						allPosts.addAll(serverDB.get(hashID));
		    					}
		    				}
		    			}
		    		}
		    		if (allPosts.isEmpty() == false) {
		    			//System.out.println("Posts exist");
		    			for (ForumPost f: allPosts) {
				    		String outComment = f.getHashID()+ "-" + f.getID().toString() + "-" + f.getUser() + "-" + f.getComment();
							outputSentence += outComment + '\n';
						}
		    			outputSentence = outputSentence.replaceAll("\n", "/n") + '\n';
		    			//System.out.println(outputSentence);
		    		} else {
		    			outputSentence = "No posts exist" + '\n';
		    		}
		    		
		    } else {
		    	outputSentence = clientSentence + '\n';
		    }
		    
		    
		    // process input
		    

		    // send reply
		    DataOutputStream outToClient = new DataOutputStream(connectionSocket.getOutputStream());
		    outToClient.writeBytes(outputSentence);
		    
		    if (words.length == 2 && words[0] != null && words[1] != null && words[0].equals("push")) {
		    	System.out.println("Waiting for connection");
	    		
	    		Socket userSocket = welcomeSocket.accept();
	    		pushUsers.add(userSocket);
	    		System.out.println("connection from " + userSocket);
		    }

		    if (words.length >= 3 && words[0] != null && words[0].equals("post_to_forum") && recentPost != null) {
			    for (Socket user: pushUsers) {
			    	System.out.println("Fowarding posts to push users");
					DataOutputStream pushOut = new DataOutputStream(user.getOutputStream());
					String pushPost = recentPost.getHashID() + "-" + recentPost.getID() + "-" + recentPost.getUser() + "-" + recentPost.getComment() + '\n';
					pushOut.writeBytes(pushPost);
				}
		    }
		    
		} 
		
		// end of while (true)

	} // end of main()

} // end of class TCPServer
