
public class Page {
	private String[] lines = new String[9];
	private int pageNumber;
	public Page (int pageNum, String[] newLines) {
		this.lines = newLines;
		this.pageNumber = pageNum;
	}
	
public Page getPage(int pageNum) {
	return this;
}

public String[] getLines() {
	return this.lines;
}
}
